﻿
namespace ReGoap.Unity.Test
{
    /// <summary>
    /// 测试记忆
    /// </summary>
    public class ReGoapTestMemory : ReGoapMemory<string, object>
    {
        public void Init()
        {
            Awake();
        }

        public void SetValue(string key, object value)
        {
            state.Set(key, value);
        }
    }
}